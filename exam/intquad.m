function Q=intquad(n)
A=ones(n);
Q=[A*-1,A*exp(1);A*pi,A];
end