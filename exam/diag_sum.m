function b=diag_sum(A)
[m,n]=size(A);
a=min(m,n);
x=(a+1)/2;
A=A([1:a],[1:a]);
B=flip(A);
if mod(a,2)==0
     b=sum(diag(A))+sum(diag(B));
else
     b=sum(diag(A))+sum(diag(B))-A(x,x);
end
end